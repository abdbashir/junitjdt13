package com.example.PracticeJunit.Service;

import com.example.PracticeJunit.Entity.ProductEntity;
import org.hibernate.service.spi.ServiceException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductService {

    Integer idHehe = 12345;
    String productName = "Hiya-hiya";
    Integer productCount = 5;

    public Optional<ProductEntity> getOneProduct(Integer productId) {
        if (!idHehe.equals(productId)) {
            throw new ServiceException("Tidak ada produk dengan ID ini");
        } else {
            ProductEntity productEntity = new ProductEntity();
            productEntity.setProductId(idHehe);
            productEntity.setProductName(productName);
            productEntity.setProductCount(productCount);

            return Optional.of(productEntity);
        }
    }

    public ProductEntity updateProduct(ProductEntity productEntity) {
        if (!idHehe.equals(productEntity.getProductId())) {
            throw new ServiceException("Produk tidak ditemukan, jadi tidak ada yang dapat diubah.");
        }

        ProductEntity existing = productEntity;

        if (productEntity.getProductName() != null) {
            existing.setProductName(productEntity.getProductName());
        }
        if (productEntity.getProductCount() != null) {
            existing.setProductCount(productEntity.getProductCount());
        }

        return existing;
    }
}
