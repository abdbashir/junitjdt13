package com.example.PracticeJunit.Response;

public class ResponseBase {

    String message;

    @Override
    public String toString() {
        return "ResponseBase{" +
                "message='" + message + '\'' +
                '}';
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
