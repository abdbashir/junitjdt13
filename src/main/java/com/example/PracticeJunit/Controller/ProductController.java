package com.example.PracticeJunit.Controller;

import com.example.PracticeJunit.Entity.ProductEntity;
import com.example.PracticeJunit.Service.ProductService;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService){
        this.productService = productService;
    }

    @GetMapping("/{productId}")
    public ResponseEntity<Optional<ProductEntity>> getProductById(@PathVariable Integer productId) {
        if (productId == null) {
            throw new ServiceException("Tidak ada product id ini");
        }

        Optional<ProductEntity> productEntity = productService.getOneProduct(productId);

        if (productEntity.isPresent()) {
            return new ResponseEntity<>(productEntity, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @PostMapping("/update-product")
    public ResponseEntity<ProductEntity> updateProduct(@RequestBody ProductEntity productEntity) {

        if (productEntity.getProductId() == null) {
            throw new ServiceException("gk ada product id ini");
        }

        ProductEntity product = productService.updateProduct(productEntity);
        return new ResponseEntity<ProductEntity>(product, HttpStatus.CREATED);
    }


}
