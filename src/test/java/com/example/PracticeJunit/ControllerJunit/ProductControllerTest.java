package com.example.PracticeJunit.ControllerJunit;

import com.example.PracticeJunit.Controller.ProductController;
import com.example.PracticeJunit.Entity.ProductEntity;
import com.example.PracticeJunit.Service.ProductService;
import org.hibernate.service.spi.ServiceException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class ProductControllerTest {

    @InjectMocks
    private ProductController productController;

    @Mock
    private ProductService productService;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetProductByIdWithValidId() {
        int validProductId = 1;
        ProductEntity mockProduct = new ProductEntity();
        mockProduct.setProductId(validProductId);
        when(productService.getOneProduct(validProductId)).thenReturn(Optional.of(mockProduct));

        ResponseEntity<Optional<ProductEntity>> responseEntity = productController.getProductById(validProductId);

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals(responseEntity.getBody().get().getProductId(), validProductId);
    }

    @Test
    public void testGetProductByIdWithInvalidId() {
        int invalidProductId = 99; // Assuming this product doesn't exist
        when(productService.getOneProduct(invalidProductId)).thenReturn(Optional.empty());

        ResponseEntity<Optional<ProductEntity>> responseEntity = productController.getProductById(invalidProductId);

        assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_FOUND);
    }


    @Test
    public void testUpdateProductWithValidData() {
        ProductEntity validProduct = new ProductEntity();
        validProduct.setProductId(1); // Assuming this ID exists
        when(productService.updateProduct(validProduct)).thenReturn(validProduct);

        ResponseEntity<ProductEntity> responseEntity = productController.updateProduct(validProduct);

        assertEquals(responseEntity.getStatusCode(), HttpStatus.CREATED);
        assertEquals(responseEntity.getBody().getProductId(), validProduct.getProductId());
    }

    @Test
    public void testUpdateProductWithInvalidData() {
        ProductEntity invalidProduct = new ProductEntity(); // Missing product ID
        when(productService.updateProduct(invalidProduct)).thenThrow(new ServiceException("gk ada product id ini"));

        ServiceException exception = org.junit.jupiter.api.Assertions.assertThrows(ServiceException.class, () -> {
            productController.updateProduct(invalidProduct);
        });

        assertEquals(exception.getMessage(), "gk ada product id ini");
    }


}
