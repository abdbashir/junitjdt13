package com.example.PracticeJunit.ServiceJunit;

import com.example.PracticeJunit.Entity.ProductEntity;
import com.example.PracticeJunit.Service.ProductService;
import org.hibernate.service.spi.ServiceException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ProductServiceTest {

    private ProductService productService;

    @BeforeEach
    public void setUp() {
        productService = new ProductService();
    }

    @Test
    public void testGetOneProductWithValidId() {
        Integer validProductId = 12345;
        Optional<ProductEntity> product = productService.getOneProduct(validProductId);

        assertTrue(product.isPresent());
        assertEquals(product.get().getProductId(), validProductId);
    }

    @Test
    public void testGetOneProductWithInvalidId() {
        Integer invalidProductId = 99999; // Assuming this product doesn't exist
        ServiceException exception = assertThrows(ServiceException.class, () -> {
            productService.getOneProduct(invalidProductId);
        });

        assertEquals(exception.getMessage(), "Tidak ada produk dengan ID ini");
    }

    @Test
    public void testUpdateProductWithValidData() {
        ProductEntity existingProduct = new ProductEntity();
        existingProduct.setProductId(12345);
        existingProduct.setProductName("Hiya-hiya");
        existingProduct.setProductCount(5);

        ProductEntity updatedProduct = new ProductEntity();
        updatedProduct.setProductId(12345);
        updatedProduct.setProductName("Updated Product Name");
        updatedProduct.setProductCount(10);

        ProductEntity result = productService.updateProduct(updatedProduct);

        assertEquals(result.getProductId(), existingProduct.getProductId());
        assertEquals(result.getProductName(), updatedProduct.getProductName());
        assertEquals(result.getProductCount(), updatedProduct.getProductCount());
    }

    @Test
    public void testUpdateProductWithInvalidId() {
        ProductEntity invalidProduct = new ProductEntity();
        invalidProduct.setProductId(99999); // Assuming this product doesn't exist

        ServiceException exception = assertThrows(ServiceException.class, () -> {
            productService.updateProduct(invalidProduct);
        });

        assertEquals(exception.getMessage(), "Produk tidak ditemukan, jadi tidak ada yang dapat diubah.");
    }
}
